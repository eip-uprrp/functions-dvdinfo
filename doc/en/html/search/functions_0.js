var searchData=
[
  ['getmoviebyname',['getMovieByName',['../movie_8h.html#aa2ca3be9f1d904e9f177faa425745236',1,'movie.cpp']]],
  ['getmoviebyposition',['getMovieByPosition',['../movie_8h.html#aebd1da153f25f87661ac630b47737097',1,'movie.cpp']]],
  ['getmoviegenre',['getMovieGenre',['../movie_8h.html#aaa2e1332e82b931cf45663e755cc2302',1,'movie.cpp']]],
  ['getmovieinfo',['getMovieInfo',['../movie_8h.html#ac0080d980b2f73f9eaa089f8eddeba60',1,'movie.h']]],
  ['getmoviename',['getMovieName',['../movie_8h.html#a5b13bb4e2035100dcf27abad21a2e862',1,'movie.cpp']]],
  ['getmovierating',['getMovieRating',['../movie_8h.html#af50291017de186a0a663e8418d65b7ef',1,'movie.cpp']]],
  ['getmovieyear',['getMovieYear',['../movie_8h.html#ad378998f3ab769b47a1bbc4672496f45',1,'movie.cpp']]]
];
